$(document).ready(function () {

    $('#category').on('change', (e) => {
        if ($("#category").val() == 2) {
            $('.tebak-nama-container').show()
            $('.tebak-gambar-container').hide()
        } else {
            $('.tebak-gambar-container').show()
            $('.tebak-nama-container').hide()
        }
    })
});