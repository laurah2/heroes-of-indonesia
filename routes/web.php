<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    $data['page_title'] = 'Dashboard';
    $data['page_description'] = 'Dashboard';
    $data['page_dashboard'] = 'active';
    $parent_dashboard = 'Dashboard';
    $title = 'Story';
    return view('dashboard', ['title' => $title, 'parent_dashboard' => $parent_dashboard])->with($data);
});
Route::get('/login', function () {
    return view('login');
});

// routes manajemen story
Route::get('/kisah', 'StoryController@index');
Route::get('/kisah/detail/{id}', 'StoryController@detail');
Route::get('/kisah/tambah', 'StoryController@create');
Route::get('/kisah/ubah/{id}', 'StoryController@update');

// routes manajemen quiz
Route::get('/kuis/tebak-nama', 'QuizController@pageNameQuiz');
Route::get('/kuis/tebak-gambar', 'QuizController@pageImageQuiz');
Route::get('/kuis/tambah', 'QuizController@create');
