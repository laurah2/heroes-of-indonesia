<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//story
Route::post('/story/datatable', 'Api\StoryApiController@datatable');
Route::post('/story/contribute', 'Api\StoryApiController@contribute');
Route::post('/story/create', 'Api\StoryApiController@create');
Route::post('/story/update', 'Api\StoryApiController@update');
Route::post('/story/validation', 'Api\StoryApiController@validation');
Route::post('/story/delete', 'Api\StoryApiController@delete');

//quiz
Route::post('/quiz/datatable', 'Api\QuizApiController@datatable');
Route::post('/quiz/create', 'Api\QuizApiController@create');
Route::post('/quiz/update', 'Api\QuizApiController@update');
Route::post('/quiz/delete', 'Api\QuizApiController@delete');
Route::post('/quiz/getQuiz', 'Api\QuizApiController@getQuiz');
Route::post('/quiz/validationAnswer', 'Api\QuizApiController@validation');

//area
Route::post('/area/datatable', 'Api\MstAreaApiController@datatable');
Route::post('/area/list-select2', 'Api\MstAreaApiController@listSelect2');