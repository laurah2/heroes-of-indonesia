<?php

namespace App\Http\Controllers\Api\AddForms;

use App\Http\Controllers\Api\AddForms\BaseForm;

class StoryContributateAddForm extends BaseForm{

    public function __construct($request){
        $this->fields = [
            [
                'name' => 'kategori',
                'label' => 'Judul', 
                'value' => value_from_request($request, 'kategori'),
                'validate' => true
            ],
            [
                'name' => 'judul',
                'label' => 'Judul', 
                'value' => value_from_request($request, 'judul'),
                'validate' => true
            ],
            [
                'name' => 'nama_contributor',
                'label' => 'Judul', 
                'value' => value_from_request($request, 'nama_contributor'),
                'validate' => true
            ],
            [
                'name' => 'cerita',
                'label' => 'Judul', 
                'value' => value_from_request($request, 'cerita'),
                'validate' => true
            ],
            [
                'name' => 'area_id',
                'label' => 'Deskripsi', 
                'value' => value_from_request($request, 'area_id'),
                'validate' => false
            ]
        ];
    }

}