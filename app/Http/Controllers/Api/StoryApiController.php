<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

use App\Model\MstStory;

use App\Http\Controllers\Api\QueryFilters\StoryQueryFilter;
use App\Http\Controllers\Api\AddForms\StoryAddForm;
use App\Http\Controllers\Api\AddForms\StoryContributateAddForm;

use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class StoryApiController extends BaseApiController
{
    public function datatable(Request $request)
    {
        $qf = new StoryQueryFilter($request->all());
        $query = MstStory::orderBy('updated_at', 'desc');

        if ($qf->get_search_text() != null) {
            $query = $query->where(function($q) use ($qf){
                $q->where("judul", "like", "%{$qf->get_search_text()}%")
                    ->orWhere("nama_pahlawan", "like", "%{$qf->get_search_text()}%")
                    ->orWhere("nama_contributor", "like", "%{$qf->get_search_text()}%");
            });
        }

        if ($qf->get_id() != null) {
            $query = $query->where("id", $qf->get_id());
        }

        if ($qf->get_area_id() != null) {
            $query = $query->where("area_id", $qf->get_area_id());
        }

        if ($qf->get_kategori() != null) {
            $query = $query->where("kategori", $qf->get_kategori());
        }

        if ($qf->is_active()) {
            $query = $query->where('is_aktif', 1)->where('is_validasi', 1);
        }

        $count = $query->count();
        $result = $query->limit($qf->get_length())->offset($qf->get_start());

        $data = $this->set_datatable_response($qf->get_draw(), $count, $result->get());
        return $this->success_response_datatable($data);
    }

    public function contribute(Request $request)
    {
        $rules = [
            'kategori' => 'required|in:daerah,nasional',
            'judul' => 'required',
            'nama_pahlawan' => 'required',
            'nama_contributor' => 'required',
            'cerita' => 'required',
            'area_id' => 'required|exists:mst_areas,id',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray()[0], 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $result = MstStory::create($data_fields);
            DB::commit();

            return $this->success_response($data_fields);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function create(Request $request)
    {
        $rules = [
            'kategori' => 'required|in:daerah,nasional',
            'judul' => 'required',
            'nama_pahlawan' => 'required',
            'nama_contributor' => 'nullable',
            'cerita' => 'required',
            'is_aktif' => 'required|boolean',
            'area_id' => 'nullable|exists:mst_areas,id',
            'gambar' => 'nullable|image',
            'audio' => 'nullable',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray(), 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $data_fields['nama_contributor'] = "ADMIN CIMORI";
            $image_name = null;
            $audio_name = null;
            if ($request->hasFile('gambar')) {
                $file = $request->file('gambar');
                $image_name = time() . rand(1, 1000) . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('upload/gambar/'), $image_name);
                $data_fields['gambar'] = $image_name;
            }else{
                $image_source = public_path().'/image/default/foto_def.png';
                $filename = time() . rand(1, 1000) . '.png';
                $image_name = public_path() . '/upload/gambar/' . $filename;
                $data_fields['gambar'] = $filename;
                File::copy($image_source, $image_name);
            }

            if ($request->hasFile('audio')) {
                $file = $request->file('audio');
                $audio_name = time() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('upload/audio/'), $image_name);
                $data_fields['audio'] = $audio_name;
            }
            $result = MstStory::create($data_fields);
            DB::commit();

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required|exists:mst_stories,id',
            'kategori' => 'required|in:daerah,nasional',
            'judul' => 'required',
            'nama_pahlawan' => 'required',
            'nama_contributor' => 'required',
            'cerita' => 'required',
            'is_aktif' => 'required',
            'area_id' => 'required|exists:mst_areas,id',
            'gambar' => 'nullable|image',
            'audio' => 'nullable',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails())
            return response($validator->messages()->toArray(), 400);

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $image_name = null;
            $audio_name = null;
            if ($request->hasFile('gambar')) {
                $file = $request->file('gambar');
                $image_name = time() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('upload/gambar/'), $image_name);
                $data_fields['gambar'] = $image_name;
            }
            if ($request->hasFile('audio')) {
                $file = $request->file('audio');
                $audio_name = time() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path('upload/audio/'), $image_name);
                $data_fields['audio'] = $audio_name;
            }
            $result = MstStory::update($data_fields)->where('id', $data_fields['id']);
            DB::commit();

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function validation(Request $request)
    {
        $rules = [
            'id' => 'required|exists:mst_stories,id',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails())
            return response($validator->messages()->toArray(), 400);

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $result = MstStory::where('id', $data_fields['id'])->first();
            $result->is_validasi = true;
            $result->save();
            DB::commit();

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function delete(Request $request)
    {
        $rules = [
            'id' => 'required|exists:mst_stories,id',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails())
            return response($validator->messages()->toArray(), 400);

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            $result = MstStory::where('id', $data_fields['id'])->first();
            $result->delete();
            DB::commit();

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }
}
