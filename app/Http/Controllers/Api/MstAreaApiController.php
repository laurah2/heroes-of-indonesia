<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

use App\Http\Controllers\Api\QueryFilters\AreaQueryFilter;
use App\Model\MstArea;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class MstAreaApiController extends BaseApiController
{
    public function datatable(Request $request)
    {
        $qf = new AreaQueryFilter($request->all());
        $query = new MstArea();

        if ($qf->get_search_text() != null) {
            $query = $query->where("name", "like", "%{$qf->get_search_text()}%");
        }

        if ($qf->get_id() != null) {
            $query = $query->where("id", $qf->get_id());
        }

        $count = $query->count();
        $result = $query->limit($qf->get_length())->offset($qf->get_start());

        $data = $this->set_datatable_response($qf->get_draw(), $count, $result->get());
        return $this->success_response_datatable($data);
    }

    public function listSelect2(Request $request){
        $areas = MstArea::select(['id', 'area_name as text'])->get();
        return response()->json($areas, 200);
    }
}
