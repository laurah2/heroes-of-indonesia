<?php

namespace App\Http\Controllers\Api\QueryFilters;
use App\Http\Controllers\Api\QueryFilters\BaseQueryFilter;
class StoryQueryFilter extends BaseQueryFilter{

    private $area_id;
    private $kategori;
    private $active;

    function __construct($request){
        parent::__construct($request);
        $this->area_id = isset($request['area_id']) ? $request['area_id'] : null;
        $this->active = isset($request['active']) ? $request['active'] : false;
    }

    public function get_area_id(){
        return $this->area_id;
    }

    public function get_kategori(){
        return $this->kategori;
    }

    public function is_active(){
        return $this->active;
    }
}