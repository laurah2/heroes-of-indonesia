<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

use App\Model\MstQuestion;
use App\Model\MstAnswer;

use App\Http\Controllers\Api\QueryFilters\QuizQueryFilter;

use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class QuizApiController extends BaseApiController
{
    public function datatable(Request $request)
    {
        $qf = new QuizQueryFilter($request->all());
        $query = MstQuestion::with(['answer_data']);

        if ($qf->get_search_text() != null) {
            $query = $query->where("judul", "like", "%{$qf->get_search_text()}%");
        }

        if ($qf->get_id() != null) {
            $query = $query->where("id", $qf->get_id());
        }

        if ($qf->get_kategori() != null) {
            $query = $query->where("kategori", $qf->get_kategori());
        }

        $count = $query->count();
        $result = $query->limit($qf->get_length())->offset($qf->get_start());

        $data = $this->set_datatable_response($qf->get_draw(), $count, $result->get());
        return $this->success_response_datatable($data);
    }

    public function getQuiz(Request $request)
    {
        $var = true;
        $result = null;
        $count = MstQuestion::count();

        for(;$var;){
            $id_picker = rand(1, $count);
            $result = MstQuestion::with(['answer_data' => function($q){
                $q->select('jawaban');
            }])->where('id', $id_picker)->first();

            if($result != null && $result->deleted_at == null){
                $var=false;
            }
        }
        return $this->success_response($result);
    }

    public function validation(Request $request)
    {
        $rules = [
            'id' => 'required|exists:mst_question,id',
            'answer_id' => 'required|exists:mst_answer,id'
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray()[0], 400);
        }

        $result = MstAnswer::where('question_id', $data_fields['id'])->where('id', $data_fields['answer_id'])->first();
        $jawaban = false;
        if($result->is_jawaban){
            $jawaban = true;
        }

        return $this->success_response($jawaban);
    }

    public function create(Request $request){
        $rules = [
            'kategori' => 'required|in:gambar,teks',
            'judul' => 'required',
            'gambar' => 'required',
            'answers.*.jawaban' => 'required',
            'answers.*.is_jawaban' => 'required',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray()[0], 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            if($data_fields['kategori'] == 'gambar'){
                if ($request->hasFile('gambar')) {
                    $file = $request->file('gambar');
                    $image_name = time() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('upload/gambar-quiz/'), $image_name);
                    $data_fields['gambar'] = $image_name;
                }
            }
            $question = MstQuestion::create($data_fields);
            
            foreach($data_fields['answer'] as $answer){
                if($data_fields['kategori'] == 'teks'){
                    $file = $answer->jawaban;
                    $image_name = time() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('upload/gambar-quiz/'), $image_name);
                    $answer->jawaban = $image_name;
                }
                $temp = [
                    'question_id' => $question->id,
                    'jawaban' => $answer->jawaban,
                    'is_jawaban' => $answer->is_jawaban,
                ];
                $answers[] = $temp;
            }

            MstAnswer::insert($answers);
            DB::commit();

            $result = [
                $question,
                'answer_data' => $answers
            ];

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required|exists:mst_question,id',
            'kategori' => 'required|in:gambar,teks',
            'judul' => 'required',
            'gambar' => 'required',
            'answers.*.jawaban' => 'required',
            'answers.*.is_jawaban' => 'required',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray()[0], 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            if($data_fields['kategori'] == 'gambar'){
                if ($request->hasFile('gambar')) {
                    $file = $request->file('gambar');
                    $image_name = time() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('upload/gambar-quiz/'), $image_name);
                    $data_fields['gambar'] = $image_name;
                }
            }
            $question = MstQuestion::update($data_fields)->where('id', $data_fields['id']);
            MstAnswer::where('question_id', $data_fields['id'])->delete();

            foreach($data_fields['answer'] as $answer){
                if($data_fields['kategori'] == 'teks'){
                    $file = $answer->jawaban;
                    $image_name = time() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('upload/gambar-quiz/'), $image_name);
                    $answer->jawaban = $image_name;
                }
                $temp = [
                    'question_id' => $question->id,
                    'jawaban' => $answer->jawaban,
                    'is_jawaban' => $answer->is_jawaban,
                ];
                $answers[] = $temp;
            }

            MstAnswer::insert($answers);
            DB::commit();

            $result = [
                $question,
                'answer_data' => $answers
            ];

            return $this->success_response($result);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }

    public function delete(Request $request){
        $rules = [
            'id' => 'required|exists:mst_question,id',
        ];

        $data_fields = $request->all();
        $validator = Validator::make($data_fields, $rules);

        if ($validator->fails()) {
            return response($validator->messages()->toArray()[0], 400);
        }

        Log::info(var_export($request->toArray(), TRUE));
        DB::beginTransaction();
        try {
            MstQuestion::where('id', $data_fields['id'])->delete();
            MstAnswer::where('question_id', $data_fields['id'])->delete();
            DB::commit();

            return $this->success_response();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage() . ' | File: ' . $e->getFile() . ' | Line: ' . $e->getLine());

            return $this->error_response($e->getMessage());
        }
    }
}
