<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstQuestion extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'kategori',
        'judul',
        'gambar',
        'deleted_at'
    ];

    public function answer_data()
    {
        return $this->hasMany(MstAnswer::class, 'question_id');
    }
}
