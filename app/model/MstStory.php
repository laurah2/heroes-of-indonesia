<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstStory extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'kategori',
        'judul',
        'nama_pahlawan',
        'area_id',
        'is_validasi',
        'is_aktif',
        'cerita',
        'audio',
        'gambar',
        'deleted_at'
    ];

    public function area_data()
    {
        return $this->hasOne(MstArea::class, 'id');
    }
}
