<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstAnswer extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'question_id',
        'jawaban',
        'is_jawaban',
    ];

    public function question_data()
    {
        return $this->belongsTo(MstAnswer::class);
    }
}
