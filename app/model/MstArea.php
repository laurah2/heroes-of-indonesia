<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class MstArea extends Model
{
    protected $fillable = [
        'area_name'
    ];
}
