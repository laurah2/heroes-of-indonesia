@extends('homebase')
@section('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="{{ asset("materialize/css/select2-materialize.css") }}"  media="screen,projection"/>
  <style>
    .verified{
      font-size: 12pt;
      line-height: 0px !important;
    }
    .select2-container{
      height: 46px;
    }

    .select2-selection, .select2-container{
      background-color: #FAFAFA !important;
    }
  </style>
@endsection

@section('content')
  <div class="row" style="margin-bottom: 5px">
    <div class="nav-wrapper right">
      <div class="col s12">
        <a href="#!" class="breadcrumb">{{ $parent_dashboard }}</a>
        <a href="#!" class="breadcrumb">Manajemen Kisah</a>
      </div>
    </div>
  </div>
  <div class="card-panel">
    <div class="row" style="margin-bottom: 0">
      <div class="input-field col s3">
        <a href="/kisah/tambah" class="btn btn-large waves-effect waves-light" type="button">
          Add <i class="material-icons right">add</i>
        </a>
      </div>
      <div class="input-field col s3 push-s3">
        <select id="filter-category">
          <option value="" selected>Semua Kategori</option>
          <option value="nasional">Nasional</option>
          <option value="daerah">Daerah</option>
        </select>
        <label>Kategori</label>
      </div>
      <div class="input-field col s3 push-s3">
        <input id="filter-search" type="text" class="validate" placeholder="Search Nama Kontributor">
        <label for="filter-search">Search</label>
      </div>
    </div>
    <div class="row" id="card-container">
      {{-- <div class="col m12 l4">
        <div class="card sticky-action">
          <div class="card-image waves-effect waves-block">
            <img class="activator" src="{{ asset("image/default/")}}/sample.jpg">
          </div>
          <div class="card-content">
            <span class="card-title activator grey-text text-darken-4"><i class="material-icons verified" title="This story has been validated">verified_user</i> Jendral Fang Sindo<i class="material-icons right">more_vert</i></span>
            <p>I am a very simple card. I am good at containing small bits of information.
            I am convenient because I require little markup to use effectively.</p>
          </div>
          <div class="card-reveal">
            <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
            <p>Here is some more information about this product that is only revealed once clicked on.</p>
          </div>
          <div class="card-action" style="text-align: right">
            <div class="switch left" style="margin-top: 5px">
              <label>
                <input onclick="validating(this, 1)" type="checkbox">
                <span class="lever"></span>
                Validate
              </label>
            </div>
            <div class="buttons">
              <button class="waves-effect red darken-3 btn" onclick="deleteStory(this, 1)" data-judul="Jendral Fang Sindo"><i class="material-icons">delete_forever</i></button>
            </div>
          </div>
        </div>
      </div>
      <div class="col m12 l4">
        <div class="card sticky-action">
          <div class="card-image waves-effect waves-block">
            <img class="activator" src="{{ asset("image/default/sample.jpg")}}">
          </div>
          <div class="card-content">
            <span class="card-title activator grey-text text-darken-4">Pahlawan Trip<i class="material-icons right">more_vert</i></span>
            <p>I am a very simple card. I am good at containing small bits of information.
            I am convenient because I require little markup to use effectively.</p>
          </div>
          <div class="card-reveal">
            <span class="card-title grey-text text-darken-4">Pahlawan Trip<i class="material-icons right">close</i></span>
            <p>Here is some more information about this product that is only revealed once clicked on.</p>
          </div>
          <div class="card-action" style="text-align: right">
            <a href="/kisah/2" class="waves-effect blue accent-4 btn"><i class="material-icons">remove_red_eye</i></a>
            <a href="/kisah/ubah/2" class="waves-effect yellow accent-4 btn"><i class="material-icons">edit</i></a>
            <button class="waves-effect red darken-3 btn" onclick="deleteStory(this, 1)" data-judul="Pahlawan Trip"><i class="material-icons">delete_forever</i></button>
          </div>
        </div>
      </div> --}}
    </div>
    <div class="row pagination">
      <ul class="pagination right" id="pagination">
        <li class="disabled"><a href="#"><i class="material-icons">chevron_left</i></a></li>
        <li class="active"><a href="#">1</a></li>
        <li class="waves-effect"><a href="#">2</a></li>
        <li class="waves-effect"><a href="#">3</a></li>
        <li class="waves-effect"><a href="#">4</a></li>
        <li class="waves-effect"><a href="#">5</a></li>
        <li class="waves-effect"><a href="#"><i class="material-icons">chevron_right</i></a></li>
      </ul>
    </div>
  </div>
  
<!-- components -->
<div class="col m12 l4 hide" id="story-card-clonable">
  <div class="card sticky-action">
    <div class="card-image waves-effect waves-block">
      <img class="activator logo" src="">
    </div>
    <div class="card-content">
      <span class="card-title activator grey-text text-darken-4 card-header">
      </span>
      <p class="short-desc"></p>
    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4 card-header-detail"></span>
      <p class="long-desc"></p>
    </div>
    <div class="card-action" style="text-align: right">
      <div class="switch left" style="margin-top: 5px">
        <label>
          <input onclick="" class="validating" type="checkbox">
          <span class="lever"></span>
          Validate
        </label>
      </div>
      <div class="buttons">
        <button class="waves-effect red darken-3 btn btn-delete-card" onclick="" data-judul=""><i class="material-icons">delete_forever</i></button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.2/jquery.twbsPagination.min.js"></script>
  <script type="text/javascript" src="{{ asset("materialize/js-custom/story/index.js") }}"></script>
  <script>
    var DOMAIN = "{{ asset("upload/gambar/")}}/";
    var DEFAULT = "{{ asset("image/default/foto_def.png")}}";
  </script>
@endsection