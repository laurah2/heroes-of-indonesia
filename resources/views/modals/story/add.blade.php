<div class="row" style="margin-bottom: 0px; text-align: left;">
    <a class="waves-effect btn-flat" id="button-back">
      <strong>
        <i class="material-icons left" >chevron_left</i> Button
      </strong>
    </a>
  </div>
  <div class="row" style="margin-bottom: 5px">
    <div class="nav-wrapper right">
      <div class="col s12">
        <a href="#!" class="breadcrumb">First</a>
        <a href="#!" class="breadcrumb">Second</a>
        <a href="#!" class="breadcrumb">Third</a>
      </div>
    </div>
  </div>