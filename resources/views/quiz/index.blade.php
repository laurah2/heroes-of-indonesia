@extends('homebase')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/material-components-web/4.0.0/material-components-web.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.material.min.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="{{ asset("materialize/css-custom/custom.css") }}" media="screen,projection" />
<style>
  .mdc-data-table {
    display: block;
  }

  .mdc-button {
    color: #00b0ff !important;
  }

  .mdc-button--raised:not(:disabled),
  .mdc-button--unelevated:not(:disabled) {
    color: var(--mdc-theme-on-primary, #fff) !important;
    background-color: #00b0ff !important;
  }

  .mdc-button:disabled {
    color: rgba(0, 0, 0, .37) !important;
  }

  div.dataTables_wrapper .mdc-data-table__header-cell {
    border: 1px solid rgba(0, 0, 0, 0.12) !important;
  }
</style>
@endsection

@section('content')
<div class="row" style="margin-bottom: 5px">
  <div class="nav-wrapper right">
    <div class="col s12">
      <a href="#!" class="breadcrumb">{{ $parent_dashboard }}</a>
      <a href="#!" class="breadcrumb">Manajemen Kuis</a>
      <a href="#!" class="breadcrumb">{{ $title }}</a>
    </div>
  </div>
</div>
<div class="card-panel">
  <div class="row">
    <div class="float-left">
      <a href="/kuis/tambah" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Tambah</a>
    </div>
    <div class="input-field col s3 push-s9">
      <i class="material-icons prefix">search</i>
      <input id="icon_search" type="tel" class="validate">
      <label for="icon_search">Search Nama Kontributor</label>
    </div>
  </div>
  <div class="row">
    <div class="col s12">
      <table id="example" class="mdl-data-table cell-border" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Kategori</th>
            <th>Gambar</th>
            <th>Jawaban</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>System Architect</td>
            <td>Edinburgh</td>
            <td>61</td>
            <td>
              <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>...</a>
              <ul id='dropdown1' class='dropdown-content'>
                <li><a href="#!">Edit</a></li>
                <li class="divider" tabindex="-1"></li>
                <li><a href="#!" onclick="deleteQuiz(this, 1)">Hapus</a></li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Accountant</td>
            <td>Tokyo</td>
            <td>63</td>
            <td>
              <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>...</a>
              <ul id='dropdown1' class='dropdown-content'>
                <li><a href="#!">Edit</a></li>
                <li class="divider" tabindex="-1"></li>
                <li><a href="#!" onclick="deleteQuiz(this, 1)">Hapus</a></li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.material.min.js"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable({
      processing: true,
      autoWidth: false,
      // serverSide: true,
      bLengthChange: false,
      searching: false,
      responsive: true,
      bSort: false,
      // language: {
      //     emptyTable: "Data tidak tersedia",
      //     zeroRecords: "Tidak ada data yang ditemukan",
      //     infoFiltered: "",
      //     infoEmpty: "",
      //     paginate: {
      //         previous: "‹",
      //         next: "›"
      //     },
      //     info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ Tebak Gambar",
      //     aria: {
      //         paginate: {
      //             previous: "Previous",
      //             next: "Next"
      //         }
      //     }
      // },
      // columns: [{
      //         data: null,
      //         "width": "5%"
      //     },
      //     {
      //         data: "name",
      //         "width": "40%"
      //     },
      //     {
      //         data: null
      //     }
      // ],
      columnDefs: [{
          "width": "5%",
          "targets": 0
        },
        {
          "width": "20%",
          "targets": 1
        },
        {
          "width": "20%",
          "targets": 2
        },
        {
          "width": "20%",
          "targets": 3
        },
        {
          "width": "5%",
          "targets": 4
        },
      ],
    });
  });

  function deleteQuiz(event, id) {
    swal({
        title: `Apakah yakin anda menghapus data ini?`,
        text: "Data yang dihapus tidak bisa dikembalikan!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal(`Poof! Your data has been deleted! `, {
            icon: "success",
          });
        } else {
          swal("Hufft~ Your data is safe!");
        }
      });
  }
</script>
@endsection