<header>
	<nav>
		<div class="nav-wrapper light-blue accent-3">
			<div class="row">
				<div class="col s12">
					<a href="#" data-target="sidenav" class="left sidenav-trigger hide-on-medium-and-up"><i class="material-icons">menu</i></a>
					<a href="https://codepen.io/collection/nbBqgY" target="_blank" class="brand-logo">HEROQ</a>
				</div>
			</div>
		</div>
	</nav>
</header>