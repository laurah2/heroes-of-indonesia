<aside>
    <ul id="sidenav" class="sidenav sidenav-fixed" style="transform: translateX(0px);">
        <li class="no-padding">
            <div class="logo" style="padding: 20px; padding-bottom: 0px; text-align: center;">
                <img class="background" src="{{ asset("image/default/logo.png") }}" style="width: 150px; height: 150px;">
            </div>
        </li>
        <div class="divider" id="divider-sidebar"></div>
        <li class="bold {{@$page_dashboard}}"><a href="/" class="waves-effect waves-blue">Dashboard</a></li>
        <li class="bold {{@$page_story}}"><a href="/kisah" class="waves-effect waves-blue">Kisah Pahlawan</a></li>
        <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold {{@$page_quiz}}">
                <a class="collapsible-header waves-effect waves-blue" tabindex="0" style="padding-left: 32px;">Kuis</a>
                <div class="collapsible-body">
                    <ul>
                        <li class="{{@$page_name_quiz}}" ><a class="collapsible-link" href="/kuis/tebak-nama">Kuis Tebak Nama</a></li>
                        <li class="{{@$page_image_quiz}}" ><a class="collapsible-link" href="/kuis/tebak-gambar">Kuis Tebak Gambar</a></li>
                    </ul>
                </div>
            </li>
          </ul>
        </li>
    </ul>
</aside>